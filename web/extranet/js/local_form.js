var mapa_comp;

function carregarMapa(latitude,longitude){

	$('#addr').locationpicker({
		location: {
			latitude: isNaN(latitude)?0:latitude,
			longitude: isNaN(longitude)?0:longitude
		},
		radius: 0,
		zoom: 18,
		enableAutocomplete: true,
		inputBinding: {
			latitudeInput: $('#Local_latitude'),
			longitudeInput: $('#Local_longitude')
		}
	});
}

function updateControls(addressComponents) {
	$('#Local_enrereco').val(addressComponents.streetName+','+addressComponents.streetNumber+','+addressComponents.district+','+addressComponents.city+','+addressComponents.stateOrProvince+','+addressComponents.postalCode);
}


$('#endereco-localizar').on('click',function(e){
	e.preventDefault();
	var endereco_completo = $('#Local_endereco').val();

	// var endereco_completo = complemento+", "+numero+","+bairro+","+cidade+" - "+estado+","+cep;

	$('#addr').locationsearch(endereco_completo);
	return false;
});

$('#endereco-localizar-atual').on('click', function(e){
	e.preventDefault();
	navigator.geolocation.getCurrentPosition(
		function(position) {
			carregarMapa(position.coords.latitude,position.coords.longitude);
		},
		function() {
			carregarMapa(0,0);
		}
	);
	return true;
});

$(document).ready(function(){
	var latitude = $('#Local_latitude').val();
	var longitude = $('#Local_longitude').val();

	if(latitude != ""){
		carregarMapa(latitude,longitude);
	}
	else{
		navigator.geolocation.getCurrentPosition(
			function(position) {
				carregarMapa(position.coords.latitude,position.coords.longitude);
			},
			function() {
				carregarMapa(0,0);
			}
		);
	}

});

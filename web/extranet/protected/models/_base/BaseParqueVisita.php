<?php

/**
 * This is the model base class for the table "parque_visita".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "ParqueVisita".
 *
 * Columns in table "parque_visita" available as properties of the model,
 * and there are no model relations.
 *
 * @property integer $idparque
 * @property string $nome
 * @property string $foto
 * @property string $cidade
 * @property string $texto
 * @property string $ativo
 *
 */
abstract class BaseParqueVisita extends GxActiveRecord {
	
    
    public $foto_delete;
	    
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'parque_visita';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'ParqueVisita|ParqueVisitas', $n);
	}

	public static function representingColumn() {
		return array('nome');
	}

	public function rules() {
		return array(
			array('nome, cidade', 'length', 'max'=>100),
			array('foto', 'length', 'max'=>120),
			array('ativo', 'length', 'max'=>1),
			array('texto', 'safe'),
			array('foto', 'file', 'types'=>'jpg,png', 'allowEmpty'=>true),
			array('foto_delete', 'length', 'max'=>1),
			array('nome, foto, cidade, texto, ativo', 'default', 'setOnEmpty' => true, 'value' => null),
			array('idparque, nome, foto, cidade, texto, ativo', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'idparque' => Yii::t('app', 'Idparque'),
			'nome' => Yii::t('app', 'Nome'),
			'foto' => Yii::t('app', 'Foto'),
			'cidade' => Yii::t('app', 'Cidade'),
			'texto' => Yii::t('app', 'Texto'),
			'ativo' => Yii::t('app', 'Ativo'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('idparque', $this->idparque);
		$criteria->compare('nome', $this->nome, true);
		$criteria->compare('foto', $this->foto, true);
		$criteria->compare('cidade', $this->cidade, true);
		$criteria->compare('texto', $this->texto, true);
		$criteria->compare('ativo', $this->ativo, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
}
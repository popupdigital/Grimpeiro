<?php

/**
 * This is the model base class for the table "agenda".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Agenda".
 *
 * Columns in table "agenda" available as properties of the model,
 * and there are no model relations.
 *
 * @property integer $idagenda
 * @property string $ip
 * @property string $data
 * @property string $nome
 * @property string $email
 * @property string $telefone
 * @property string $data_agendamento
 * @property string $numero_pessoas
 * @property string $interesse
 * @property string $mensagem
 * @property string $ativo
 *
 */
abstract class BaseAgenda extends GxActiveRecord {
	
    
        
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'agenda';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'Agendamentos', $n);
	}

	public static function representingColumn() {
		return array('nome');
	}

	public function rules() {
		return array(
			array('ip, nome, email, telefone, interesse', 'length', 'max'=>100),
			array('data_agendamento, numero_pessoas', 'length', 'max'=>50),
			array('ativo', 'length', 'max'=>1),
			array('data, mensagem', 'safe'),
			array('nome, email, telefone, data_agendamento, numero_pessoas, interesse', 'required'),
			array('ip, data, nome, email, telefone, data_agendamento, numero_pessoas, interesse, mensagem, ativo', 'default', 'setOnEmpty' => true, 'value' => null),
			array('idagenda, ip, data, nome, email, telefone, data_agendamento, numero_pessoas, interesse, mensagem, ativo', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array();
	}

	public function pivotModels() {
		return array();
	}

	public function attributeLabels() {
		return array(
			'idagenda' => Yii::t('app', 'Idagenda'),
			'ip' => Yii::t('app', 'Ip'),
			'data' => Yii::t('app', 'Data'),
			'nome' => Yii::t('app', 'Nome'),
			'email' => Yii::t('app', 'Email'),
			'telefone' => Yii::t('app', 'Telefone'),
			'data_agendamento' => Yii::t('app', 'Data do Agendamento'),
			'numero_pessoas' => Yii::t('app', 'Numero Pessoas'),
			'interesse' => Yii::t('app', 'Interesse'),
			'mensagem' => Yii::t('app', 'Mensagem'),
			'ativo' => Yii::t('app', 'Ativo'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('idagenda', $this->idagenda);
		$criteria->compare('ip', $this->ip, true);
		$criteria->compare('data', $this->data, true);
		$criteria->compare('nome', $this->nome, true);
		$criteria->compare('email', $this->email, true);
		$criteria->compare('telefone', $this->telefone, true);
		$criteria->compare('data_agendamento', $this->data_agendamento, true);
		$criteria->compare('numero_pessoas', $this->numero_pessoas, true);
		$criteria->compare('interesse', $this->interesse, true);
		$criteria->compare('mensagem', $this->mensagem, true);
		$criteria->compare('ativo', $this->ativo, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
	
	public function afterSave(){
		$message = new YiiMailMessage;
		$message->view = 'agenda';
		$message->setBody(array('agenda' => $this),'text/html','latin1');
		$message->subject = 'Agendamento de visita'.' - '.date('d/m/Y H:i:s');
		$message->addTo(Yii::app()->params['email_agenda']);
		$message->setReplyTo($this->email);
		$message->setFrom(array('noreply@popupdigital.com.br'  => Yii::app()->name));
		Yii::app()->mail->send($message);
	}
	
}
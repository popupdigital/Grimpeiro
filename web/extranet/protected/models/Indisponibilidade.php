<?php

Yii::import('application.models._base.BaseIndisponibilidade');

class Indisponibilidade extends BaseIndisponibilidade
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    
    
    public function init(){
		$this->data = date('d/m/Y');
  
    }
    
    public function beforeSave(){
		if($this->data != "")
			$this->data = Util::formataDataBanco($this->data);
		//{{beforeSave}}
		return parent::beforeSave();
	}
	
	public function afterFind(){
		if($this->data != "")
			$this->data = Util::formataDataApp($this->data);
		//{{afterFind}}
		return parent::afterFind();
	}
    
    public function behaviors(){
    	return array(
        	//{{behaviors}}
        );
    }
    
        
}
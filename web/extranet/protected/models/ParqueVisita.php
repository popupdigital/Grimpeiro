<?php

Yii::import('application.models._base.BaseParqueVisita');

class ParqueVisita extends BaseParqueVisita
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    
    
    public function init(){
  
    }
    
    public function beforeSave(){
		if($this->foto != "")
				unset($this->foto);
		//{{beforeSave}}
		return parent::beforeSave();
	}
	
	public function afterFind(){
		//{{afterFind}}
		return parent::afterFind();
	}
    
    public function behaviors(){
    	return array(
			'Foto' => array(
				  'class' => 'ext.behaviors.AttachmentBehavior',
				  'attribute' => 'foto',
				  'fallback_image' => 'img/imagem_nao_cadastrada.png',
				  'attribute_delete' => 'foto_delete',
				  /*
				  'attribute_size' => 'foto_tamanho',
				  'attribute_type' => 'foto_tipo',
				  'attribute_ext' => 'foto_ext',
				  */
				  'path' => "uploads/:model/foto_:id_".time().".:ext",
				  'styles' => array(
					  'p' => '200x200',
					  'g' => '800x800',
				)          
			),
        	//{{behaviors}}
        );
    }
    
        
}
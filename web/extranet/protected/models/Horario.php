<?php

Yii::import('application.models._base.BaseHorario');

class Horario extends BaseHorario
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}


	public function init(){

	}

	public function beforeSave(){
		//{{beforeSave}}
		return parent::beforeSave();
	}

	public function afterFind(){
		//{{afterFind}}
		return parent::afterFind();
	}

	public function behaviors(){
		return array(
			//{{behaviors}}
		);
	}
	public function getDia(){
		$dias = Util::getDiasSemana();
		return $dias[$this->dia_semana];
	}



}

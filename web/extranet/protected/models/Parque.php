<?php

Yii::import('application.models._base.BaseParque');

class Parque extends BaseParque
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    
    
    public function init(){
  
    }
    
    public function beforeSave(){
		if($this->foto_destaque != "")
				unset($this->foto_destaque);
		//{{beforeSave}}
		return parent::beforeSave();
	}
	
	public function afterFind(){
		//{{afterFind}}
		return parent::afterFind();
	}
    
    public function behaviors(){
    	return array(
			'FotoDestaque' => array(
				  'class' => 'ext.behaviors.AttachmentBehavior',
				  'attribute' => 'foto_destaque',
				  'fallback_image' => 'img/imagem_nao_cadastrada.png',
				  'attribute_delete' => 'foto_destaque_delete',
				  /*
				  'attribute_size' => 'foto_destaque_tamanho',
				  'attribute_type' => 'foto_destaque_tipo',
				  'attribute_ext' => 'foto_destaque_ext',
				  */
				  'path' => "uploads/:model/foto_destaque_:id_".time().".:ext",
				  'styles' => array(
					  'p' => '200x200',
					  'g' => '800x800',
				)          
			),
			'galeria' => array(
				'class' => 'GalleryBehavior',
				'idAttribute' => 'gallery_id',
				'versions' => array(
					'small' => array(
						'centeredpreview' => array(200, 200),
					),
					'medium' => array(
						'resize' => array(800, null),
					)
				),
				'name' => false,
				'description' => true,
			 )
        	//{{behaviors}}
        );
    }
    
        
}
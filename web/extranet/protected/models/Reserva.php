<?php

Yii::import('application.models._base.BaseReserva');

class Reserva extends BaseReserva
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}


	public function init(){
		$this->data = date('d/m/Y');
		$this->data_solicitado = date('d/m/Y H:i:s');

	}

	public function beforeSave(){
		if($this->data != "")
		$this->data = Util::formataDataBanco($this->data);
		if($this->data_solicitado != "")
		$this->data_solicitado= Util::formataDataHoraBanco($this->data_solicitado);
		//{{beforeSave}}
		return parent::beforeSave();
	}

	public function afterFind(){
		if($this->data != "")
		$this->data = Util::formataDataApp($this->data);
		if($this->data_solicitado != "")
		$this->data_solicitado = Util::formataDataHoraApp($this->data_solicitado);
		//{{afterFind}}
		return parent::afterFind();
	}

	public function behaviors(){
		return array(
			//{{behaviors}}
		);
	}

	//status
	public function getStatusArray(){
		return array(
			'solicitado' => 'Solicitada',
			'confirmado' => 'Confirmada',
			'finalizado' => 'Finalizada',
			'cancelado' => 'Cancelada',
			'nao-utilizada' => 'N�o utilizada',
		);
	}
	public function getStatus(){
		$array = $this->getStatusArray();
		return $array[$this->status];
	}

	public function getStatusLabelArray(){
		return array(
			'solicitado' => 'warning',
			'confirmado' => 'success',
			'finalizado' => 'info',
			'cancelado' => 'danger',
			'nao-utilizada' => 'default',
		);
	}


	public function temDisponibilidade($dados){//idlocal, idhorario, data, matricula

		$criteria = new CDbCriteria();
		$criteria->addCondition("idhorario = '".$dados['idhorario']."'");
		$criteria->addCondition("data = '".$dados['data']."'");
		$indisponiveis = Indisponibilidade::model()->findAll($criteria);
		if($indisponiveis){
			return false;
		}

		$local = Local::model()->findByPk($dados['idlocal']);

		$criteria = new CDbCriteria();
		$criteria->addCondition("t.idlocal = '".$dados['idlocal']."'");
		$criteria->addCondition("t.idhorario = '".$dados['idhorario']."'");
		$criteria->addCondition("t.data = '".$dados['data']."'");
		$criteria->addCondition("t.status <> 'cancelado'");
		$reservas = Reserva::model()->count($criteria);

		if($reservas > 0){
			return false;
		}
		return true;

	}


	public function getHorariosExtranet(){

		$dia = date('w', strtotime(Util::formataDataBanco($this->data)));

		$criteria = new CDbCriteria();
		$criteria->addCondition("idlocal = '".$this->idlocal."'");
		$criteria->addCondition("dia_semana = '".$dia."'");
		$horarios = Horario::model()->findAll($criteria);
		return $horarios;

	}

	public function notificar(){
		try {

			$message = new YiiMailMessage;
			$message->view = 'reserva';
			$message->setBody(array('model' => $this),'text/html','latin1');
			$message->subject = ' Reserva #'.$this->idreserva.' - '.date('d/m/Y H:i:s');
			$message->addTo('agendamento@grimpeiro.com.br');
			$message->addTo($this->email);
			$message->setFrom(array(Yii::app()->mail->transportOptions['username'] => Yii::app()->name));

			if(Yii::app()->mail->send($message))
			return true;

		} catch (\Exception $e) {

			return false;
		}

		return false;
	}



}

<?php

class HorarioController extends GxController {


	public function getRepresentingFields(){
		return Horario::representingColumn();
	}

	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Horario'),
		));
	}

	public function actionCreate($idlocal) {
		$model = Local::model()->findByPk($idlocal);

		$horario = new Horario();

		if (isset($_POST['Horario'])) {
			$horario->setAttributes($_POST['Horario']);
			$erro = '';
			if(!is_array($_POST['dia_semana'])){
				$horario->addError('dia_semana', "Escolha o(s) dia(s) da semana");
				$erro = CHtml::errorSummary($horario);
				$has_erro = true;
			}else{
				$has_erro = false;
				$erro = '';
				$glue_marker = '';
				$dias_da_semana = Util::getDiasSemana();
				foreach($_POST['dia_semana'] as $dia_semana){

					$valida = new Horario();
					$valida->setAttributes($_POST['Horario']);
					$valida->dia_semana = $dia_semana;
					$valida->ativo = '1';
					if($valida->validate()){

						$salvar[] = $valida;
					}else{

						$erro = $erro.$glue_marker.'<p><strong>'.$dias_da_semana[$dia_semana].'</strong></p>'.CHtml::errorSummary($valida);
						$has_erro = true;
					}
					$glue_marker = '<br>';


				}


				if(!$has_erro){
					foreach ($salvar as $sched) {
						$sched->save();
					}
					$this->redirect($this->createUrlRel('local/view',array('id' => $model->idlocal,'success'=>'create-horario')));
				}

			}

		}


		$this->render('//local/consulta', array( 'model' => $model, 'horario'=>$horario, 'erro'=>$erro, 'tab'=>'horarios'));
	}

	public function actionIndisponivel($idlocal) {
		$model = Local::model()->findByPk($idlocal);

		$model_ind = new Indisponibilidade();


		$model_ind->idlocal = $idlocal;
		$model_ind->horarios = array();
		$model_salvar = array();
		$erros = false;
		if (isset($_POST['Indisponibilidade'])) {
			$model_ind->setAttributes($_POST['Indisponibilidade']);
			$model_ind->horarios = is_array($_POST['Indisponibilidade']['horarios'])?$_POST['Indisponibilidade']['horarios']:array();
			if($model_ind->horarios){
				foreach ($model_ind->horarios as $idhorario) {
					$indisponivel = new Indisponibilidade();
					$indisponivel->idlocal = $idlocal;
					$indisponivel->idhorario = $idhorario;
					$indisponivel->data = $model_ind->data;

					if($indisponivel->validate()){
						$model_salvar[] = $indisponivel;
					}else{
						$erros = true;
						$model_ind->addError('horarios', CHtml::errorSummary($indisponivel));
					}

				}
			}

			if(!$erros){
				foreach ($model_salvar as $indisponivel) {
					$indisponivel->save();
				}
				$this->redirect($this->createUrlRel('local/view',array('id' => $model_ind->idlocal,'success'=>'update-horario')));
			}
			$erro = CHtml::errorSummary($model_ind);
		}

		$this->render('//local/indisponibilidade', array( 'model' => $model, 'model_ind'=>$model_ind, 'erro'=>$erro));
	}

	public function actionGetHorarios($idlocal, $data) {

		$dia_sem = date('w', strtotime(Util::formataDataBanco($data)));
		// echo $dia_sem;
		$criteria = new CDbCriteria();
		$criteria->addCondition("idlocal = '".$idlocal."'");
		$criteria->addCondition("dia_semana = '".$dia_sem."'");
		$horarios = Horario::model()->findAll($criteria);

		if($horarios){
			foreach($horarios as $horario) {
				?>
				<div class="">
					<label for="<?=$horario->idhorario;?>">
						<input type="checkbox" name="Indisponibilidade[horarios][]" value="<?=$horario->idhorario;?>" id="<?=$horario->idhorario;?>">
						<?=$horario->hora_inicio; ?> at&eacute; <?=$horario->hora_termino; ?>
					</label>
				</div>
				<?php
			}
		}else{
			?>
			<em>Nenhum hor&aacute;rio para o dia escolhido.</em>
			<?php
		}

	}
	public function actionListaHorarios($idlocal, $data) {
		$model = new Reserva();
		$model->data = $data;
		$model->idlocal = $idlocal;
		$this->renderPartial("//reserva/horarios",array(
			'model' => $model,
		));

	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Horario');

		if (isset($_POST['Horario'])) {
			$model->setAttributes($_POST['Horario']);

			if ($model->save()) {
				$this->redirect($this->createUrlRel('view',array('id' => $model->idhorario,'success'=>'update')));
			}
		}

		$this->render('update', array(
			'model' => $model,
		));
	}

	public function actionDelete($id) {
		$model = $this->loadModel($id, 'Horario');
		if($_GET['confirm'] == 1){
			$model->delete();
			if($_GET['ajax'] == 1){
				echo CJSON::encode(array('sucesso' => '1'));
				Yii::app()->end();
			}
			else
			$this->redirect($this->createUrlRel('index'));
		}
		else{
			$this->renderPartial("//site/delete_console", array(
				'model' => $model,
			));
		}
	}
	public function actionAtivar($id){
		$model=$this->loadModel($id);
		$model->ativo= 1;
		$model->update();
		Yii::app()->request->redirect(Yii::app()->user->returnUrl);
	}

	public function actionDesativar($id){
		$model=$this->loadModel($id);
		$model->ativo= 0;
		$model->update();
		Yii::app()->request->redirect(Yii::app()->user->returnUrl);
	}

	public function loadModel($id)
	{
		$model=Horario::model()->findByPk($id);
		if($model===null)
		throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function actionIndex() {
		$criteria = new CDbCriteria;

		//C�dgio de busca
		if(isset($_GET['q'])){
			$model = new Horario();
			$atributos = $model->tableSchema->columns;

			foreach($atributos as $att){
				if(!$att->isPrimaryKey && !$att->isForeignKey)
				$criteria->addCondition($att->name." like '%".$_GET['q']."%'", "OR");
			}
		}

		if(isset($_GET['o']) && isset($_GET['f']) ){
			$criteria->order = $_GET['f']." ".$_GET['o'];
		}
		else{
			$criteria->order = 'idhorario desc';
		}

		if(count($this->rel_conditions) > 0){
			foreach($this->rel_conditions as $field => $value){
				$criteria->addCondition($field." = '".$value."'");
			}
		}

		$dataProvider = new CActiveDataProvider('Horario', array(
			'criteria'=>$criteria,
			'pagination' => array(
				'pageSize'=> Yii::app()->user->pageSize,
				'pageVar'=>'p',
			),
		));

		$this->render('index', array(
			'dataProvider' => $dataProvider,
			'model' => Horario::model(),
		));
	}

	public function afterAction($action){
		Yii::app()->user->returnUrl = Yii::app()->request->requestUri;
		return parent::afterAction($action);
	}

	public function beforeAction($action){
		/*
		if(is_numeric($_GET['idlinha'])){
		$linha = Linha::model()->findByPk($_GET['idlinha']);
		$this->rel_conditions['idlinha'] = $_GET['idlinha'];
		$this->rel_link['idlinha'] = $_GET['idlinha'];
		if(Yii::app()->user->obj->group->temPermissaoAction('linha','index')){
		$this->breadcrumbs[$linha->label(2)] = array('linha/index');
		$this->breadcrumbs[$linha->nome] = array('linha/view','id'=>$linha->idlinha);
	}
	else{
	$this->breadcrumbs[] = Linha::label(2);
	$this->breadcrumbs[] = $linha->nome;
}
}
*/

return parent::beforeAction($action);
}

}

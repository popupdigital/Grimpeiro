<?php

class SiteController extends Controller {

    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
            'uploadStreaming' => array(
                'class' => 'xupload.actions.XUploadAction',
                'path' => Yii::app()->getBasePath() . "/../uploads",
                'publicPath' => Yii::app()->getBaseUrl() . "/uploads",
            ),
        );
    }
	public function actionGetCidades(){
		$cidades = Cidade::model()->findAll(array(
			'condition' => "idestado = '".$_GET['idestado']."'",
			'order' => 'nome',
		));
		$cidades_list = CHtml::listData($cidades,'idcidade','nome');
		echo CJSON::encode($cidades_list);
		Yii::app()->end();
		
	}

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
        if (Yii::app()->user->isGuest) {
            $this->redirect("login");
        }
		else{
        	$this->render('index');
		}
	}
    
    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }


    /**
     * Displays the login page
     */
    public function actionLogin() {
        if (!Yii::app()->user->isGuest)
            $this->redirect("site");
        $model = new LoginForm;

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login()) {
				if(!empty(Yii::app()->user->returnUrl))
					$this->redirect(Yii::app()->user->returnUrl);
				else
                	$this->redirect("site/index");
            }
        } elseif (isset($_GET['email']) && isset($_GET['senha'])) {
            $model->email = $_GET['email'];
            $model->senha = $_GET['senha'];
            if ($model->login($senha_encript = true))
                $this->redirect('usuario/update?id=' . Yii::app()->user->obj->idUser);
        }
        // display the login form
        $this->render('login', array('model' => $model));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout() {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

    public function actionEnviar_link_senha() {
        $model = new RecuperarSenhaForm;

        $sucesso = NULL;

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'recuperar-senha-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['RecuperarSenhaForm'])) {
            $model->attributes = $_POST['RecuperarSenhaForm'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->enviarEmail())
                $sucesso = 1;
        }
        // display the login form
        $this->render('enviar_link_senha', array('model' => $model, 'sucesso' => $sucesso));
    }

}

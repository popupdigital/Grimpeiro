<?php
if(Yii::app()->user->obj->group->temPermissaoAction($this->id,'index')){
	$this->breadcrumbs[$model->label(2)] = array('index');
}
else{
	$this->breadcrumbs[] = $model->label(2);
}
if($this->hasRel()){
	$this->breadcrumbs[$model->label(2)] = array('rel'=>$this->getRel());
}
$this->breadcrumbs[] = Yii::t('app','Visualizar');

if(isset($_GET['success'])){
	if($_GET['success'] == 'create-horario'){
		$tab = 'horarios';
		$msg = "Horário(s) cadastrado(s) com sucesso!";
	}elseif($_GET['success'] == 'update-horario'){
		$tab = 'horarios';
		$msg = "Horário(s) atualizado(s) com sucesso!";
	}else{
		$msg = "Cadastro atualizado com sucesso!";
		if($_GET['success'] == 'create'){
			$msg = "Cadastro realizado com sucesso! Cadastre os horários de reserve na aba 'Horários'";
		}
	}
}

?>
<div class="row-fluid">
	<div class="span12">

		<ul class="nav nav-tabs nav-justified">
			<li class="<?=$tab!='horarios'?'active':''?>">
				<a href="#dados" data-toggle="tab" aria-expanded="false">
					<i class="fa fa-home"></i>
					Dados
				</a>
			</li>
			<li class="<?=$tab=='horarios'?'active':''?>">
				<a href="#horarios" data-toggle="tab" aria-expanded="false">
					<i class="fa fa-clock-o"></i>
					Horarios
				</a>
			</li>
			<li class="">
				<a href="#reservas" data-toggle="tab" aria-expanded="false">
					<i class="fa fa-calendar"></i>
					Reservas
				</a>
			</li>

		</ul>

		<div class="w-box">


			<div class="tab-content">
				<div class="tab-pane <?=$tab!='horarios'?'active':''?>" id="dados">
					<div>

						<?php
						$this->renderPartial("//layouts/sucesso",array(
							'success' => $_GET['success'],
							'msg' => $msg,
						));
						$this->renderPartial("//local/view",array(
							'model' => $model,
						));
						?>

					</div>
				</div>
				<div class="tab-pane <?=$tab=='horarios'?'active':''?>" id="horarios">
					<?
					if(Yii::app()->user->obj->group->temPermissaoAction('horario','index')){
						?>
						<div class="w-box-header">
							<h4>Horários</h4>
						</div>
						<div class="w-box-content" style="padding:50px;">

							<p>
								<strong>Cadastrar horário</strong>
							</p>

							<form method="post" action="<?php echo $this->createUrlRel('horario/create',array('idlocal'=>$model->getPrimaryKey()));?>">

								<?php
								if($erro){
									?>
									<div class="alert alert-danger">
										<?=$erro;?>
									</div>
									<?php
								}
								?>

								<input type="hidden" name="Horario[idlocal]" value="<?= $model->idlocal?>" />

								<label style="margin-top:20px;">Selecione os dias da semana</label>
								<div class="dias "style="margin-top:15px;">
									<?
									$dias_da_semana = Util::getDiasSemana();
									foreach($dias_da_semana as $i => $dia){
										?>
										<div class="pull-left" style="width:108px; font-size:9px; text-align: center;line-height:15px;">
											<label for="dia<?=$i?>" class=" control-label">
												<input style="margin:0;" type="checkbox" class="dia_checkbox <?=($i>0 && $i<6) ? 'dia_util_checkbox' : '';?> <?=($i == 0 || $i == 6) ? 'dia_final_semana_checkbox' : '';?>" id="dia<?=$i?>" name="dia_semana[]" <?=( (is_array($_POST['dia_semana'])) && (in_array($i,$_POST['dia_semana'])) ) ? 'checked="checked"' : '' ;?>  value="<?=$i?>">
												<?=Util::formataTexto($dia);?>
											</label>
										</div>
										<?
									}
									?>
								</div>
								<div class="clearfix"> </div>
								<div class="row-fluid" style="margin-top:15px;">
									<div class="span5">
										<label >
											Hora de In&iacute;cio
										</label>
										<div class="input-group">
											<input class="input-xlarge mask-hora" placeholder="HH:MM:SS" name="Horario[hora_inicio]" value="<? echo $horario->hora_inicio; ?>" type="text">

										</div>
									</div>
									<div class="span5">
										<label >
											Hora de T&eacute;rmino
										</label>
										<div class="input-group">
											<input class="input-xlarge mask-hora" placeholder="HH:MM:SS" name="Horario[hora_termino]" value="<? echo $horario->hora_termino; ?>" type="text">

										</div>

									</div>
									<div class="span2">
										<button class="btn btn-default" style="margin-top:25px;" type="submit">Cadastrar</button>
									</div>
								</div>
							</form>

							<hr>

							<?php
							if(Yii::app()->user->obj->group->temPermissaoAction('horario','indisponivel')){
								?>
								<div class="pull-right">
									<a href="<?php echo $this->createUrlRel('horario/indisponivel',array('idlocal'=>$model->getPrimaryKey()));?>" class="btn btn-danger"><i class="fa fa-calendar-minus-o"></i> Adicionar indisponibilidade</a>
								</div>
								<?php
							}
							?>
							<p>
								<strong>Horários disponíveis</strong>
							</p>

							<table class="table">
								<?php
								$autorizacao_horario = Yii::app()->user->obj->group->temPermissaoController('horario');

								foreach($dias_da_semana as $i => $dia){
									?>
									<thead>
										<th colspan="3" ><?=Util::formataTexto($dia);?></th>
									</thead>
									<?php
									$criteria = new CDbCriteria();
									$criteria->addCondition("idlocal = '".$model->idlocal."'");
									$criteria->addCondition("dia_semana = '".$i."'");
									$horarios = Horario::model()->findAll($criteria);

									if($horarios){

										foreach($horarios as $horario) {
											?>
											<tr data-id="<?=$horario->idhorario;?>">
												<td width="10"><i class="fa fa-clock-o"></i></td>
												<td width="650">
													<?=$horario->hora_inicio; ?> at&eacute; <?=$horario->hora_termino; ?>
													<?php
													$criteria = new CDbCriteria();
													$criteria->addCondition("idhorario = '".$horario->idhorario."'");
													$criteria->addCondition("data >= '".date('Y-m-d')."'");
													$criteria->order = 'data asc';
													$indisponiveis = Indisponibilidade::model()->findAll($criteria);
													if($indisponiveis){
														foreach ($indisponiveis as $indisponivel) {
															echo '<p class="m-l-10"><em>Indisponível no dia '.$indisponivel->data.'</em></p>';
														}
													}
													?>

												</td>
												<td class="td-operacoes">
													<?php

													if($autorizacao_horario){
														?>
														<a class="btn btn-default" href="<?php echo $this->createUrlRel('horario/view',array('id'=>$horario->getPrimaryKey()));?>"><i class="fa fa-cogs"></i></a>
														<?
													}

													?>

												</td>
											</tr>

											<?php
										}
									}
									else{
										?>
										<tr>
											<td colspan="3"><i>Nenhum hor&aacute;rio cadastrado!</i></td>
										</tr>
										<?php

									}
								}

								?>
							</table>
						</div>

						<?
					}
					?>

				</div>
				<div class="tab-pane" id="reservas">
					<?
					if(Yii::app()->user->obj->group->temPermissaoAction('reserva','index')){
						?>

						<div class="w-box-header">
							<h4>Reservas</h4>
						</div>
						<div class="w-box-content" style="padding:50px;">
							<?php
							if(count($model->reservas) > 0){
								echo GxHtml::openTag('ul');
								foreach($model->reservas as $relatedModel) {
									echo GxHtml::openTag('li');
									echo GxHtml::link(GxHtml::encode(GxHtml::valueEx($relatedModel)), array('reserva/view', 'id' => GxActiveRecord::extractPkValue($relatedModel, true)));
									echo GxHtml::closeTag('li');
								}
								echo GxHtml::closeTag('ul');
							}
							else{
								echo '<i>Nenhum registro encontrado</i>';
							}
							?>
						</div>

						<?
					}
					?>
				</div>
			</div>



		</div>

	</div>
</div>

<?php
Yii::app()->clientScript->registerScriptFile('https://maps.googleapis.com/maps/api/js?key=AIzaSyCkhJjMWNMgZeEgPRvAZbj-sYDR7KYBZuU&sensor=false&libraries=places',2);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl .'/js/locationpicker.jquery.js',2);
?>
<script type="text/javascript">
$(document).ready( function(){
	$('#addr').locationpicker({
		location: {
			latitude: <?=$model->latitude?>,
			longitude: <?=$model->longitude?>,
		},
		zoom:17,
		radius: 0,
		inputBinding: {
			latitudeInput: $('#addr-lat'),
			longitudeInput: $('#addr-lon'),
			locationNameInput: $('#addr-address')
		},
		enableAutocomplete: false
	});
});

</script>

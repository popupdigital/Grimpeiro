<?php
if(Yii::app()->user->obj->group->temPermissaoAction($this->id,'index')){
	$this->breadcrumbs[$model->label(2)] = array('index');
}
else{
	$this->breadcrumbs[] = $model->label(2);
}
if($this->hasRel()){
	$this->breadcrumbs[$model->label(2)] = array('rel'=>$this->getRel());
}
$this->breadcrumbs[] = Yii::t('app','Cadastrar');

$form = new GxActiveForm();
?>
<div class="row-fluid">
	<div class="span12">
		<div class="w-box">
			<div class="w-box-header">
				<h4><?=Yii::t('app','Cadastrar');?></h4>
			</div>
			<div class="w-box-content">

				<form method="post" action="<?php echo $this->createUrlRel('horario/indisponivel',array('idlocal'=>$model->getPrimaryKey()));?>">

					<?php
					if($erro){
						?>
						<div class="alert alert-danger">
							<?=$erro;?>
						</div>
						<?php
					}
					?>

					<input type="hidden" class="local-busca" name="Indisponibilidade[idlocal]" value="<?= $model_ind->idlocal?>" />

					<div class="formSep">
						<dl class="dl-horizontal">
							<dt>
								<?php echo $form->labelEx($model,'data'); ?>
							</dt>
							<dd>
								<?php $this->widget('CJuiDateTimePicker',array(
									'model'=>$model_ind, //Model object
									'attribute'=>'data', //attribute name
									'language' => 'pt',
									'mode'=>'date', //use 'time','date' or 'datetime' (default)
									'htmlOptions' =>array(
										'class' => 'form-control data-indisp',
									),
									'options'=>array(
										'readonly' => 'readonly',
										'changeYear' => true,
										'changeMonth' => true,
									)

								)); ?>
							</dd>
						</dl>
					</div>




					<div class="formSep">
						<dl class="dl-horizontal">
							<dt>
								<?php echo $form->labelEx($model,'horarios'); ?>
							</dt>
							<dd>
								<div class="horarios-dia">
									<?php
									$dia_semana = date('w', strtotime(Util::formataDataBanco($model_ind->data)));
									$criteria = new CDbCriteria();
									$criteria->addCondition("idlocal = '".$model_ind->idlocal."'");
									$criteria->addCondition("dia_semana = '".($dia_semana)."'");
									$horarios = Horario::model()->findAll($criteria);

									if($horarios){

										foreach($horarios as $horario) {
											$check = '';
											if(in_array($horario->idhorario, $model_ind->horarios)){
												$check = 'checked';
											}
											?>
											<div class="">
												<label for="<?=$horario->idhorario;?>">
													<input type="checkbox" name="Indisponibilidade[horarios][]" value="<?=$horario->idhorario;?>" id="<?=$horario->idhorario;?>">
													<?=$horario->hora_inicio; ?> at&eacute; <?=$horario->hora_termino; ?>
												</label>
											</div>
											<?php
										}
									}else{
										?>
										<em>Nenhum hor�rio para o dia escolhido.</em>
										<?php
									}
									?>
								</div>
							</dd>
						</dl>
					</div>


					<div class="formSep">
						<dl class="dl-horizontal">
							<dt></dt>
							<dd>
								<button class="btn btn-success"  type="submit"><?=Util::formataTexto("Salvar");?></button>
							</dd>
						</dl>
					</div>



				</form>
			</div>
		</div>
	</div>
</div>

<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/indisponibilidade.js', CClientScript::POS_END);
?>

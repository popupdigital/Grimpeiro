<div class="form">

	<?php $form = $this->beginWidget('GxActiveForm', array(
		'id' => 'local-form',
		'enableAjaxValidation' => false,
		'htmlOptions'=> array (
			'class' => 'form-horizontal',
			'enctype' => 'multipart/form-data',
			'action' => $this->createUrlRel($this->action->id),
		)
	));
	?>

	<?php
	$this->renderPartial("//layouts/erros",array(
		'model' => $model,
	));
	?>

	<div class="formSep">
		<dl class="dl-horizontal">
		  <dt><?php echo $form->labelEx($model,'idparque',array('class'=>'control-label')); ?>
	</dt>
		  <dd>
			<?php echo $form->dropDownList($model, 'idparque', GxHtml::listDataEx(ParqueVisita::model()->findAllAttributes(null, true)), array('class' => 'input-xxlarge','empty'=>'Selecione...')); ?>
				</dd>
	   </dl>
	</div>
	<!-- row -->


	<div class="formSep">
		<dl class="dl-horizontal">
			<dt><?php echo $form->labelEx($model,'nome',array('class'=>'control-label')); ?>
			</dt>
			<dd>
				<?php echo $form->textField($model, 'nome', array('maxlength' => 100,'class' => 'input-xxlarge')); ?>
			</dd>
		</dl>
	</div>
	<!-- row -->

	<div class="formSep">
		<dl class="dl-horizontal">
			<dt><?php echo $form->labelEx($model,'descricao',array('class'=>'control-label')); ?>
			</dt>
			<dd>
				<?php echo $form->textArea($model, 'descricao',array('rows'=>'10','class'=>'input-xxlarge')); ?>
			</dd>
		</dl>
	</div>
	<!-- row -->

	<div class="formSep">
    <dl class="dl-horizontal">
      <dt><?php echo $form->labelEx($model,'foto',array('class'=>'control-label')); ?> </dt>
      <dd>
        <?php
        $this->widget( 'application.extensions.Plupload.PluploadWidget', array(
          'model' => $model,
          'attribute' => 'foto',
        ) );
        ?>
      </dd>
    </dl>
  </div>

	<div class="formSep">
		<dl class="dl-horizontal">
			<dt><?php echo $form->labelEx($model,'gallery_id',array('class'=>'control-label')); ?>
			</dt>
			<dd>
				<?php $this->widget('GalleryManager', array(
					'gallery' => $model->galeria->getGallery(),
					'controllerRoute' => 'gallery',
				));
				echo $form->hiddenField($model, 'gallery_id'); ?>
			</dd>
		</dl>
	</div>
	<!-- row -->


	<div class="formSep">
		<dl class="dl-horizontal">
			<dt>
				<?php echo $form->labelEx($model,'endereco'); ?>
			</dt>
			<dd>
				<?php echo $form->textArea($model, 'endereco',array('rows'=>'2','class'=>'input-xxlarge')); ?>
			</dd>
		</dl>
	</div>

	<div class="form-group">

	</div>

	<div class="formSep">
		<dl class="dl-horizontal">
			<dt>
				Mapa
			</dt>
			<dd>
				<div class="btn-group">
					<button aria-expanded="true" data-toggle="dropdown" class="btn btn-default dropdown-toggle" type="button"><i class="fa fa-location-arrow"></i> Localizar no mapa&nbsp;&nbsp;<span class="caret"></span></button>
					<ul role="menu" class="dropdown-menu">
						<li><a href="#" id="endereco-localizar">Endere�o informado</a></li>
						<li><a href="#" id="endereco-localizar-atual">Meu endere�o atual</a></li>
					</ul>
				</div>
				<div id="addr" style="width:90%; height: 400px;"></div>

				<div class="row-fluid">
					<div class="span6">
						<?php echo $form->labelEx($model,'latitude'); ?>
						<?php echo $form->textField($model, 'latitude', array('maxlength' => 100,'class' => 'input-xlarge')); ?>
					</div>
					<div class="span6">
						<?php echo $form->labelEx($model,'longitude'); ?>
						<?php echo $form->textField($model, 'longitude', array('maxlength' => 100,'class' => 'input-xlarge')); ?>
					</div>
				</div>
			</div>
		</dd>
	</dl>
</div>
<!-- row -->

<!-- row -->

<div class="formSep">
	<dl class="dl-horizontal">
		<dt>
			<?php echo $form->labelEx($model,'limite_pessoas',array('class'=>'control-label')); ?>
		</dt>
		<dd>
			<?php echo $form->textField($model, 'limite_pessoas', array('class' => 'input-xxlarge')); ?>
		</dd>
	</dl>
</div>
<!-- row -->

<div class="formSep">
	<dl class="dl-horizontal">
		<dt><?php echo $form->labelEx($model,'ativo',array('class'=>'control-label')); ?>
		</dt>
		<dd>
			<?php echo $form->checkBox($model, 'ativo'); ?>
		</dd>
	</dl>
</div>
<!-- row -->


<div class="formSep">
	<dl class="dl-horizontal">
		<dt>&nbsp;</dt>
		<dd>

			<button type="submit" class="btn">
				<?
				if($this->action->id == 'create'){
					?>
					<i class="icon-plus"></i>&nbsp;Cadastrar
					<?
				}
				else{
					?>
					<i class="icon-pencil"></i>&nbsp;Atualizar
					<?
				}
				?>
			</button>
			<?
			if(Yii::app()->user->obj->group->temPermissaoAction($this->id,'index')){
				?>
				<a class="btn" href="<?php echo $this->createUrlRel('index');?>"><i class="icon-chevron-left"></i> Voltar</a>
				<?
			}
			?>
			<?
			if($this->action->id == 'update' && Yii::app()->user->obj->group->temPermissaoAction($this->id,'delete')){
				?>
				<a class="btn btn-delete" href="<?php echo $this->createUrlRel('delete',array('id'=>$model->idlocal));?>" style="margin-left:30px;"><i class="icon-trash"></i> Excluir</a>
				<?
			}
			?>        </dd>
		</dl>
	</div>


	<?php
	$this->endWidget();
	?>

</div>
<!-- form -->
<?php
$cid = time();
Yii::app()->clientScript->registerScriptFile('https://maps.googleapis.com/maps/api/js?key=AIzaSyCkhJjMWNMgZeEgPRvAZbj-sYDR7KYBZuU&sensor=false&libraries=places',2);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl .'/js/locationpicker.jquery.js?cid='.$cid,2);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl .'/js/local_form.js?cid='.$cid,2);
?>




	<div class="w-box-header">
		<h4>Visualizar</h4>
	</div>
	<div class="w-box-content">


		<div class="formSep">
		  <dl class="dl-horizontal">
			<dt><?=Util::formataTexto($model->getAttributeLabel('idparqueVisita'));?></dt>
			<dd><?=($model->parqueVisita !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->parqueVisita)), array('parqueVisita/view', 'id' => GxActiveRecord::extractPkValue($model->parqueVisita, true)),array('class' => 'relational-link')) : null)?></dd>
		  </dl>
		</div>
		<div class="formSep">
			<dl class="dl-horizontal">
				<dt><?=Util::formataTexto($model->getAttributeLabel('nome'));?></dt>
				<dd><?=Util::formataTexto($model->nome)?></dd>
			</dl>
		</div>
		<div class="formSep">
			<dl class="dl-horizontal">
				<dt><?=Util::formataTexto($model->getAttributeLabel('descricao'));?></dt>
				<dd><?=Util::formataTexto($model->descricao)?></dd>
			</dl>
		</div>
		<div class="formSep">
			<dl class="dl-horizontal">
				<dt>
					<?=Util::formataTexto($model->getAttributeLabel('foto'));?>
				</dt>
				<dd>
					<img src="<?php echo Yii::app()->request->baseUrl; ?>/uploads/Local/<?=$model->foto;?>" width="200" >
				</dd>
			</dl>
		</div>
		<div class="formSep">
			<dl class="dl-horizontal">
				<dt><?=Util::formataTexto($model->getAttributeLabel('gallery_id'));?></dt>
				<dd><?php $this->widget('GalleryManager', array(
					'gallery' => $model->galeria->getGallery(),
					'controllerRoute' => 'gallery',
				));?></dd>
			</dl>
		</div>
		<div class="formSep">
			<dl class="dl-horizontal">
				<dt><?=Util::formataTexto($model->getAttributeLabel('endereco'));?></dt>
				<dd><?=Util::formataTexto($model->endereco)?></dd>
			</dl>
		</div>
		<div class="formSep">
			<dl class="dl-horizontal">
				<dt>
					Confira a localiza��o
				</dt>
				<dd>
					<div id="addr" style="width: 100%; height: 400px;"></div>
					<br>
					<strong><?=Util::formataTexto($model->getAttributeLabel('latitude'));?></strong>:<?=Util::formataTexto($model->latitude)?>
					<br>
					<strong><?=Util::formataTexto($model->getAttributeLabel('longitude'));?></strong>:<?=Util::formataTexto($model->longitude)?>
				</dd>
			</dl>
		</div>

		<div class="formSep">
			<dl class="dl-horizontal">
				<dt><?=Util::formataTexto($model->getAttributeLabel('limite_pessoas'));?></dt>
				<dd><?=Util::formataTexto($model->limite_pessoas)?></dd>
			</dl>
		</div>

		<div class="formSep">
			<dl class="dl-horizontal">
				<dt><?=Util::formataTexto($model->getAttributeLabel('ativo'));?></dt>
				<dd><?=Util::formataTexto($model->ativo ? 'Sim' : 'N�o')?></dd>
			</dl>
		</div>

		<div class="formSep">
			<dl class="dl-horizontal">
				<dt>&nbsp;</dt>
				<dd>
					<?php
					if(Yii::app()->user->obj->group->temPermissaoAction($this->id,'update')){
						?>
						<a class="btn" href="<?php echo $this->createUrlRel('update',array('id'=>$model->idlocal));?>"><i class="icon-edit "></i> Editar</a>
						<?php
					}
					?>
					<?php
					if(Yii::app()->user->obj->group->temPermissaoAction($this->id,'index')){
						?>
						<a class="btn" href="<?php echo $this->createUrlRel('index');?>"><i class="icon-chevron-left"></i> Voltar</a>
						<?php
					}
					?>
					<?php
					if(Yii::app()->user->obj->group->temPermissaoAction($this->id,'delete')){
						?>
						<a class="btn btn-delete" href="<?php echo $this->createUrlRel('delete',array('id'=>$model->idlocal));?>" style="margin-left:30px;"><i class="icon-trash"></i> Excluir</a>
						<?php
					}
					?>
					  </dd>
				</dl>
			</div>

		</div>

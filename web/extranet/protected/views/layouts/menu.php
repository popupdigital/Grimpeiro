<header>
  <div class="row-fluid">
    <nav class="nav-icons">
      <div class="logo_cliente"><a href="<?php echo Yii::app()->request->baseUrl; ?>/site"><img src="<?php echo Yii::app()->request->baseUrl; ?>/img/logo_menu.png" alt=""/></a></div>
      <div class="menu_pop">
        <?
         if ( Yii::app()->user->obj->group->temPermissaoAction( 'sobre', 'index' ) ) {
        ?>
         <a href="<?=$this->createUrl('sobre/index');?>"><i class="fa fa-lg fa-caret-right"></i><span>Sobre n�s</span></a>
        <?
         }if ( Yii::app()->user->obj->group->temPermissaoAction( 'apoiador', 'index' ) ) {
        ?>
          <a href="<?=$this->createUrl('apoiador/index');?>"><i class="fa fa-lg fa-caret-right"></i><span>Apoiadores</span></a>
        <?
         }if ( Yii::app()->user->obj->group->temPermissaoAction( 'noticia', 'index' ) ) {
        ?>
          <a href="<?=$this->createUrl('noticia/index');?>"><i class="fa fa-lg fa-caret-right"></i><span>Not�cias</span></a>
        <?
          }if ( Yii::app()->user->obj->group->temPermissaoAction( 'parque', 'index' ) ) {
        ?>
          <a href="<?=$this->createUrl('parque/index');?>"><i class="fa fa-lg fa-caret-right"></i><span>O Parque</span></a>
        <?
          }if ( Yii::app()->user->obj->group->temPermissaoAction( 'agenda', 'index' ) ) {
        ?>
          <a href="<?=$this->createUrl('agenda/index');?>"><i class="fa fa-lg fa-caret-right"></i><span>Agendamentos</span></a>
        <?
          }if ( Yii::app()->user->obj->group->temPermissaoAction( 'contato', 'index' ) ) {
        ?>
          <a href="<?=$this->createUrl('contato/index');?>"><i class="fa fa-lg fa-caret-right"></i><span>Contato</span></a>
        <?
    }if ( Yii::app()->user->obj->group->temPermissaoAction( 'parqueVisita', 'index' ) ) {
        ?>
          <a href="<?=$this->createUrl('parqueVisita/index');?>"><i class="fa fa-lg fa-caret-right"></i><span>Parques</span></a>
        <?
          }if ( Yii::app()->user->obj->group->temPermissaoAction( 'local', 'index' ) ) {
        ?>
          <a href="<?=$this->createUrl('local/index');?>"><i class="fa fa-lg fa-caret-right"></i><span>Locais</span></a>
        <?
          }if ( Yii::app()->user->obj->group->temPermissaoAction( 'reserva', 'index' ) ) {
        ?>
          <a href="<?=$this->createUrl('reserva/index');?>"><i class="fa fa-lg fa-caret-right"></i><span>Reservas</span></a>
        <?
          }
        ?>
      </div>
      <div class="menu_suporte"> D&uacute;vidas ou suporte <a href="mailto:somos@alavanca.digital">atendimento@alavanca.digital</a> <a href="http://www.alavanca.digital" target="_blank">www.alavanca.digital</a> </div>
    </nav>
  </div>
</header>

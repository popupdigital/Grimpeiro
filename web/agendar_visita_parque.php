<?
/*********************************************************
*Controle de versao: 2.0
*********************************************************/
include("gzip/gzipHTML.php");
include_once("extranet/autoload.php");
$criteria = new CDbCriteria();
$criteria->addCondition("ativo = '1'");
$criteria->order = 'nome asc';
$parques = ParqueVisita::model()->findAll($criteria);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Agendar Visita - Grimpeiro - Grupo de Apoio &agrave; Gest&atilde;o do Parque Estadual das Arauc&aacute;rias</title>
<?php include("header.php"); ?>
<style type="text/css"><? echo file_get_contents ('css/formularios.css')?></style>
</head>
<body class="internas noticias">
	<div id="wrapper">
		<div id="topo"><?php include("topo.php"); ?></div>
		<div class="container conteudo">
			<div class="titulo_pagina">
				<h2>AGENDE SUA VISITA</h2>
				<h3>ESCOLHA O LOCAL DA VISITA</h3>
			</div>
			<div class="conteudo">
				<?
				foreach($parques as $parque){
					?>
					<div class="locais_lista columns eight"><h2>
						<a href="agendar-visita/trilhas?idparque=<?=$parque->idparque?>">
							<?
							if(is_file($arquivo = 'extranet/uploads/ParqueVisita/'.$parque->foto)){
								?>
								<span class="img_reserva"><img src="<?=$arquivo?>" alt="<?=$parque->nome?>"/></span>
								<?
							}
							?>
							<?php /*?><span class="local_nome"><?=$parque->nome?></span><?php */?>
							<span class="local_cidade"><?=$parque->cidade?></span>
							<?php /*?><span><?=Util::formataTexto($parque->texto)?></span><?php */?>
						</a>
					</h2></div>
					<?
				}
				?>
        <div class="clear"></div>
			</div>
		</div>
		<div>
			<?php include("rodape.php"); ?>
		</div>
	</div>
	<?php include("scripts.php"); ?>
</body>
</html>

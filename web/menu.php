<?
  $linkMenu = str_replace(explode("/",""),"",current(explode("?",$_SERVER['REQUEST_URI'])));
?>
<div class="menu">
  <div class="menuButton"></div>
  <div class="clear"></div>
  <div class="menuContent">
    <a href="inicial" class="<?=(strpos($linkMenu,"inicial")!==false?"ativado":"")?><?=(strpos($linkMenu,NULL)!==false?"ativado":"")?>">INICIAL</a>
    <a href="sobre-nos" class="<?=(strpos($linkMenu,"sobre-nos")!==false?"ativado":"")?>">SOBRE N&Oacute;S</a>
    <a href="parque" class="<?=(strpos($linkMenu,"parque")!==false?"ativado":"")?>">O PARQUE</a>
    <a href="agendar-visita" class="<?=(strpos($linkMenu,"agendar-visita")!==false?"ativado":"")?>">AGENDAR VISITA</a>
    <a href="apoiadores" class="<?=(strpos($linkMenu,"apoiadores")!==false?"ativado":"")?>">APOIADORES</a>
    <a href="noticias" class="<?=(strpos($linkMenu,"noticias")!==false?"ativado":"")?><?=(strpos($linkMenu,"noticia")!==false?"ativado":"")?>">NOT&Iacute;CIAS</a>
    <a href="localizacao" class="<?=(strpos($linkMenu,"localizacao")!==false?"ativado":"")?>">LOCALIZA&Ccedil;&Atilde;O</a>
    <a href="contato" class="<?=(strpos($linkMenu,"contato")!==false?"ativado":"")?>">CONTATO</a>
    <a href="https://www.facebook.com/ParqueEstadualdasAraucarias/" target="_blank" class="bt_facebook">f</a>
  </div>
</div>

<?php
include_once("RequestManager.php");
$rotas = array(
	'/parque'=> 'o_parque.php',
	'/sobre-nos'=> 'sobre_nos.php',
	'/agendar-visita'=> 'agendar_visita_parque.php',
	'/agendar-visita/trilhas'=> 'agendar_visita_locais.php',
	'/agendar-visita/dados'=> 'agendar_visita_form.php',
	'/agendar-visita/sucesso/(?P<idreserva>\d+)'=> 'agendar_visita_sucesso.php',
	'/apoiadores'=> 'apoiadores.php',
	'/noticias'=> 'noticias.php',
	'/noticia/(?P<noticia>\S+)/(?P<titulo>\S+)'=>'noticias-mostra.php',
	'/localizacao'=> 'localizacao.php',
	'/contato'=> 'fale_conosco.php',
	'/gethorarios'=> 'lista-horarios.php',

	//Links para inicial
	'/'=>'inicial.php',
	'/inicial'=>'inicial.php',
	'/inicial/(?P<acesso>\w+)'=>'inicial.php',
	'/index'=>'inicial.php',
	'/inicial'=>'inicial.php',
	'/(?P<url>\S+)'=>'inicial.php',
);
$request_manager = new RequestManager();
$request_manager->run($rotas);
exit;

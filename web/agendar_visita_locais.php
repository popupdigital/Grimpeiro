<?
/*********************************************************
*Controle de versao: 2.0
*********************************************************/
include("gzip/gzipHTML.php");
$idparque = $_GET['idparque'];
if(!is_numeric($idparque)){
	header("location: ".Yii::app()->baseUrl."/agendar-visita");
}
include_once("extranet/autoload.php");
$criteria = new CDbCriteria();
$criteria->addCondition("ativo = '1'");
$criteria->addCondition("idparque = '".$idparque."'");
$criteria->order = 'nome asc';
$locais = Local::model()->findAll($criteria);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Agendar Visita - Grimpeiro - Grupo de Apoio &agrave; Gest&atilde;o do Parque Estadual das Arauc&aacute;rias</title>
<?php include("header.php"); ?>
<style type="text/css"><? echo file_get_contents ('css/formularios.css');?></style>
</head>
<body link="#0563c1" vlink="#800000" class="internas noticias" dir="ltr" lang="pt-BR">
	<div id="wrapper">
		<div id="topo"><?php include("topo.php"); ?></div>
		<div class="container conteudo">
			<div class="titulo_pagina">
				<h2>AGENDE SUA VISITA</h2>
				<h3>NORMAS PARA VISITA&Ccedil;&Atilde;O DAS UCS</h3>
			</div>
      <div class="local_txt">
        <p> -&nbsp;&Eacute; obrigat&oacute;rio o uso de cal&ccedil;ado fechado e cal&ccedil;a comprida  para percorrer as trilhas;</p>
        <p>- &Eacute; recomend&aacute;vel  trazer/fazer uso de protetor solar, repelente, bon&eacute;/chap&eacute;u;</p>
        <p>- Na Unidade de  Conserva&ccedil;&atilde;o n&atilde;o s&atilde;o comercializado bebidas e alimenta&ccedil;&atilde;o, por  tanto o visitante deve vir preparado com garrafa de &aacute;gua e  suprimentos;</p>
        <p>- Se  responsabilizar por todo o&nbsp;lixo produzido, org&acirc;nico ou n&atilde;o,  retirando da Unidade de Conserva&ccedil;&atilde;o;</p>
        <p>- O visitante  deve estar atento a lagartas e animais pe&ccedil;onhentos que possam ser  encontrados no decorrer da trilha.</p>
        <p>Segundo a  PORTARIA n&ordm; 96/2021, IMA/SC, de 06/06/2021, somente ser&aacute; permitido  grupos de visitantes com o m&aacute;ximo de 7 pessoas. </p>
        <p>I &ndash; utiliza&ccedil;&atilde;o  obrigat&oacute;ria de m&aacute;scara pelos visitantes e condutores/ monitores de  visitantes, durante toda a perman&ecirc;ncia no interior da unidade de  conserva&ccedil;&atilde;o; </p>
        <p>II &ndash; aferi&ccedil;&atilde;o  da temperatura corporal por meio de term&ocirc;metros de infravermelho  antes da entrada no centro de visitantes/infraestruturas existentes  sendo que pessoas com temperatura acima de 37,8 &ordm;C dever&atilde;o ser  orientadas a procurar o servi&ccedil;o de sa&uacute;de;</p>
        <p>III &ndash;  agendamento pr&eacute;vio das visitas para os centros de visitantes/  infraestruturas existentes;</p>
        <p>IV &ndash;  cadastramento dos visitantes feito por e-mail quando do agendamento,  atrav&eacute;s de formul&aacute;rio ou, eventualmente, realizados pelos  monitores/condutores de visitantes/volunt&aacute;rios na chegada do centro  de visitantes/ infraestruturas existentes;</p>
        <p>V - grupos de  visitantes com o m&aacute;ximo de 7 pessoas;</p>
        <p>VI &ndash;  distanciamento m&iacute;nimo de 1,5 metros entre visitantes durante as  visitas;</p>
        <p>VII &ndash; hor&aacute;rio  estabelecido de atendimento nos centros de visitantes/infraestruturas  existentes;</p>
        <p>VIII &ndash; em caso  de cancelamento da visita, deve-se comunicar com anteced&ecirc;ncia m&iacute;nima  de dois dias, sob pena de impedimento de novo agendamento no prazo de  dois meses para todo o grupo envolvido.</p>
      </div>
			<div class="titulo_pagina mt60">
				<h2>AGENDE SUA VISITA</h2>
				<h3>ESCOLHA O LOCAL DA VISITA</h3>
			</div>
			<div class="conteudo">
				<?
				foreach($locais as $local){
					?>
					<h2 class="noticias_lista columns eight">
						<a href="agendar-visita/dados?idlocal=<?=$local->idlocal?>">
							<?
							  if(is_file($arquivo = 'extranet/uploads/Local/'.$local->foto)){
						  ?>
							  <img src="<?=$arquivo?>" width="100%" class="block" alt="<?=$local->nome;?>"/>
						  <?
							  }
							?>
							<span><i><?=$local->nome;?></i></span>
						</a>
					</h2>
					<?
				}
				?>
        <div class="clear"></div>
			</div>
		</div>
		<div>
			<?php include("rodape.php"); ?>
		</div>
	</div>
	<?php include("scripts.php"); ?>
</body>
</html>

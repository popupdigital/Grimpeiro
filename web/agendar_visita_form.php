<?
include("gzip/gzipHTML.php");
include_once("extranet/autoload.php");
$model = new Reserva();
$form = new CActiveForm();
$idlocal = $_GET['idlocal'];
$data = date('Y-m-d');
if(!is_numeric($idlocal)){
	header("location: ".Yii::app()->baseUrl."/agendar-visita");
}
if($_POST['Reserva']){
	$reserva_model = new Reserva();
	$idlocal = $_POST['Reserva']['idlocal'];
	$idhorario = $_POST['Reserva']['idhorario'];
	$data = ($_POST['Reserva']['data']);
	$reserva_model->setAttributes($_POST['Reserva']);
	$reserva_model->status = 'solicitado';
	$reserva_model->scenario = 'reserva';
	$horario = Horario::model()->findByPk($idhorario);
	$reserva_model->dia_semana = $horario->dia_semana;
	$reserva_model->hora_inicio = $horario->hora_inicio;
	$reserva_model->hora_termino = $horario->hora_termino;
	$reserva_model->data = Util::formataDataApp($data);
	if($reserva_model->save()){
		$reserva_model->notificar();
		header("location: ".Yii::app()->baseUrl."/agendar-visita/sucesso/".$reserva_model->idreserva);
	}
	$erro = CHtml::errorSummary($reserva_model);
}
if($idlocal){
	$local = Local::model()->findByPk($idlocal);
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Agendar Visita - Grimpeiro - Grupo de Apoio &agrave; Gest&atilde;o do Parque Estadual das Arauc&aacute;rias</title>
<?php include("header.php"); ?>
<style type="text/css"><? echo file_get_contents ('css/formularios.css');?><? echo file_get_contents ('css/slick.css');?><? echo file_get_contents ('css/fancybox.css');?></style>
</head>
<body class="internas">
	<div id="wrapper">
		<div id="topo"><?php include("topo.php"); ?></div>
		<div class="container conteudo">
			<div class="titulo_pagina">
				<h2>CONHE&Ccedil;A A TRILHA</h2>
				<h3><?=$local->nome;?></h3>
			</div>
			<div class="conteudo formulario">
				<div>
          <div class="local_txt"><?php echo $local->descricao;?></div>
					<div class="galeria mt60">
			        <?
			          $fotos = $local->galeria->getGallery()->galleryPhotos;
			          if(is_array($fotos)){
			            $total = count($fotos);
			            foreach($fotos as $i => $foto){
			        ?>
			          <div><a href="extranet/gallery/<?=$foto->id?>.jpg" data-fancybox="images"><img src="extranet/gallery/<?=$foto->id?>.jpg" width="100%" alt="<?=($noticia->titulo)?>" class="rollover"/></a></div>
				    <?
						  }
					  }
					?>
          </div>
				</div>
			</div>
      <div class="clear mt60"></div>
			<div class="titulo_pagina mt60">
        <h2>AGENDAR VISITA</h2>
        <h3>AGENDE SUA VISITA</h3>
			</div>
			<div class="conteudo formulario mt60">
        <?
          if(!empty($erro)){
        ?>
          <div class="error margin20 center columns sixteen u-full-width"><?=$erro;?></div>
        <?
          } if($_GET['sucesso'] == 1){
        ?>
        <div class="sucesso_msg columns sixteen u-full-width" style="text-align:center"> <strong>Agendamento solicitado com sucesso. Obrigado!</strong> </div>
        <?
          }
        ?>
				<div class="clear"></div>
				<div class="columns six u-full-width">
					<div class="calendar" id="table">
						<div class="header">
							<div class="month" id="month-header">
							</div>
							<div class="buttons">
								<button class="icon" onclick="prevMonth()" title="M�s anterior">
									<
								</button>
								<button class="icon" onclick="nextMonth()" title="Pr�ximo m�s">
									>
								</button>
							</div>
						</div>
					</div>
            <div class="local_aviso"><strong>Aviso:</strong> Visitas dispon&iacute;veis de quarta a domingo</div>
				</div>
				<div class="columns ten u-full-width">
					<div class="" style="width:100%;">
  				<form id="form_contato" name="form_contato" method="post" action="agendar-visita/dados?idlocal=<?=$idlocal?>" class="">
							<input type="hidden"  name="grava" value="1" />
							<?php echo $form->hiddenField($model,'idlocal', array('value'=>$idlocal,'id'=>'local-reserva')); ?>
							<?php echo $form->hiddenField($model,'data', array('value'=>$data,'id'=>'data-reserva')); ?>
							<?php echo $form->dropDownList($model,'idhorario', array(),array('class'=>'columns ten u-full-width','id'=>'lista-horarios','maxlength'=>100,'empty'=>'ESCOLHA O HOR�RIO')); ?>
							<div class="clear"></div>
							<?php echo $form->textField($model,'nome',array('class'=>'columns ten u-full-width','maxlength'=>100,'placeholder'=>'NOME')); ?>
							<?php echo $form->textField($model,'email',array('class'=>'columns ten u-full-width','maxlength'=>100,'placeholder'=>'E-MAIL')); ?>
							<div class="clear"></div>
							<?php echo $form->textField($model,'telefone',array('class'=>'columns ten u-full-width','maxlength'=>100,'placeholder'=>'TELEFONE')); ?>
							<?php echo $form->textField($model,'quantidade',array('class'=>'columns ten u-full-width','maxlength'=>100,'placeholder'=>'N�MERO DE PESSOAS')); ?>
							<div class="clear"></div>
							<button name="enviar" type="submit" value="ENVIAR" class="u-pull-right">ENVIAR</button>
							<div class="clear"></div>
						</form>

					</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
	<div>
		<?php include("rodape.php"); ?>
	</div>
</div>
<script type="text/javascript">
  <?php
    if($idhorario){
  ?>
    var idhorario_post = '<?=$idhorario?>';
  <?php
    }else{
  ?>
    var idhorario_post = '';
  <?php
    }
  ?>
</script>
<?php include("scripts.php"); ?>
<script type="text/javascript">
  <?php
    if($_POST['Reserva']['data']){
  ?>
    setDate('<?=$data?>');
  <?php
    }
  ?>
</script>
<script type="text/javascript" src="gzip/gzip.php?arquivo=../jquery/jquery.slick.min.js&amp;cid=<?=$cid?>"></script>
<script type="text/javascript" src="gzip/gzip.php?arquivo=../jquery/jquery.fancybox.min.js&amp;cid=<?=$cid?>"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $(".galeria").slick({slidesToShow: 1,slidesToScroll: 1,infinite: true,dots: true,arrows: false,});
    $('[data-fancybox]').fancybox({});})
</script>
</body>
</html>

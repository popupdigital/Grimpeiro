<?
include_once("extranet/autoload.php");
$criteriaRodape = new CDbCriteria();
$criteriaRodape->order = 'idapoiador asc';
$criteriaRodape->addCondition("ativo = 1");
$roda_apoiadores = Apoiador::model()->findAll($criteriaRodape);
?>
<div id="rodape">
  <div class="logos">
    <img src="img/logo_grimpeiro.png" width="224" height="134" alt=""/>
    <img src="img/logo_araucarias.png" width="118" height="155" alt=""/>
    <img src="img/apoiadores.png" width="28" height="149" alt=""/>
      <?
        foreach($roda_apoiadores as $roda_apoiador){
	  ?>
        <img src="extranet/uploads/Apoiador/<?=$roda_apoiador->logo?>" alt="<?=$roda_apoiador->titulo?>"/>
      <?
		}
	  ?>
  </div>
  <div class="assinatura nexalight">GRIMPEIRO &reg; 2021. Todos os direitos reservados. <a href="http://www.alavanca.digital" target="_blank">Desenvolvido por Alavanca</a></div>
</div>


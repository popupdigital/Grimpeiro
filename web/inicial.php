<?
include("gzip/gzipHTML.php");
include_once("extranet/autoload.php");
$criteriaNoticia = new CDbCriteria();
$criteriaNoticia->order = 'idnoticia desc';
$criteriaNoticia->addCondition("ativo = 1");
$criteriaNoticia->limit = 5;
$criteriaNoticia->addCondition("foto_destaque <> '' ");
$noticias = Noticia::model()->findAll($criteriaNoticia);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Grimpeiro - Grupo de Apoio &agrave; Gest&atilde;o do Parque Estadual das Arauc&aacute;rias</title>
<?php include("header.php"); ?>
<style type="text/css"><?php echo file_get_contents ('css/bxslider.css');?>
</style>
</head>
<body class="inicial">
<div id="wrapper">
  <div id="topo">
    <?php include("topo.php"); ?>
    <div class="banner container">
      <div class="columns fourteen offset-by-one frase times">Se existe o Grimpeiro, existe a Arauc&aacute;ria. Se existe Arauc&aacute;ria, existe o Grimpeiro. Um depende do outro para existir.</div>
      <div class="botao_verde nexalight"><a href="agendar-visita">AGENDE UMA VISITA</a></div>
    </div>
  </div>
  <div id="parque">
    <div class="container container_1200">
      <div class="columns four">
        <h2 class="titulo linha"><span></span>CONHE&Ccedil;A O PARQUE</h2>
        <h3 class="titulo times" style="text-transform:uppercase"></h3>
      </div>
      <div class="columns twelve offset-by-one omega alpha">
        <div><img src="img/oparque.jpg" width="100%" alt="CONHE&Ccedil;A O PARQUE"/></div>
        <div class="columns eight alpha txt nexalight">Criado pelo Decreto n&ordm; 293, de 30 de maio de 2003, localiza-se no munic&iacute;pio de S&atilde;o Domingos, na Bacia do Rio Chapec&oacute;. A &aacute;rea de 612 hectares &eacute; exclusivamente coberta por floresta ombr&oacute;fila. &Eacute; importante ressaltar a ocorr&ecirc;ncia de duas esp&eacute;cies em extin&ccedil;&atilde;o, a arauc&aacute;ria angustif&oacute;lia (arauc&aacute;ria) e dicksonia sellowiana (xaxim). Dentro do Parque encontra-se o rio jacutinga, afluente do rio bonito. Al&eacute;m de ser um importante afluente do rio Chapec&oacute;, &eacute; respons&aacute;vel pelo abastecimento de &aacute;gua do munic&iacute;pio de S&atilde;o Domingos. O Parque Estadual das Arauc&aacute;rias &eacute; a primeira unidade de conserva&ccedil;&atilde;o de arauc&aacute;rias sob a responsabilidade do Governo do Estado.</div>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
    <div class="margin40">
      <ul class="bxslider" id="slider_atracoes">
        <li><a href="parque" class="nexabold" style="background-image:url(img/slider_inicial/slider_01.jpg)"><span>ANIMAIS</span></a></li>
        <li><a href="parque" class="nexabold" style="background-image:url(img/slider_inicial/slider_02.jpg)"><span>CASCATAS</span></a></li>
        <li><a href="parque" class="nexabold" style="background-image:url(img/slider_inicial/slider_03.jpg)"><span>LAGOS</span></a></li>
        <li><a href="parque" class="nexabold" style="background-image:url(img/slider_inicial/slider_04.jpg)"><span>PLANTAS</span></a></li>
        <li><a href="parque" class="nexabold" style="background-image:url(img/slider_inicial/slider_05.jpg)"><span>TRILHAS</span></a></li>
      </ul>
      <div class="clear"></div>
    </div>
  </div>
  <div id="novidades">
	<div class="container container_1200">
	  <div class="columns six offset-by-five">
        <h2 class="titulo linha"><span></span>NOSSAS ATIVIDADES</h2>
        <h3 class="titulo times">ACOMPANHE NOSSAS ATIVIDADES</h3></div><div class="clear"></div>
      <div>
        <ul class="bxslider" id="slider_noticias">
		  <?
	        foreach($noticias as $noticia){
		  ?>
            <li>
              <div class="columns twelve alpha omega"><a href="noticia/<?=$noticia->idnoticia;?>/<?=Util::removerAcentos($noticia->titulo)?>"><img src="extranet/uploads/Noticia/<?=$noticia->foto_destaque?>" width="100%" class="rollover" alt="<?=htmlentities($sobre->titulo);?>"/></a></div>
              <div class="columns eight alpha omega">
                <div class="conteudo">
                  <h2 class="nexabold"><a href="noticia/<?=$noticia->idnoticia;?>/<?=Util::removerAcentos($noticia->titulo)?>"><?=Util::formataResumo($noticia->titulo,100)?></a></h2>
                  <div class="txt nexalight"><a href="noticia/<?=$noticia->idnoticia;?>/<?=Util::removerAcentos($noticia->titulo)?>"><?=Util::formataResumo($noticia->texto,300)?></a></div>
                </div>
              </div>
              <div class="clear"></div>
            </li>
          <?
			}
		  ?>
        </ul>
        <div class="arrows columns nineteen alpha omega"><span id="slider-prev"></span><span id="slider-next"></span></div>      
      </div>
    </div>
  </div>
  <div id="visita">
    <div class="container container_1200">
      <div class="columns thirteen explorador"><img src="img/explorador.png" alt="" width="779" /></div>
      <div class="columns seven">
        <h2 class="titulo linha"><span></span>DESFRUTE DESSA AVENTURA</h2>
        <h3 class="times">CONHE&Ccedil;A O PARQUE ESTADUAL DAS ARAUC&Aacute;RIAS DE S&Atilde;O DOMINGOS - SC</h3>
        <div class="botao_verde nexalight"><a href="agendar-visita">AGENDAR UMA VISITA</a></div>
      </div>
      <div class="clear"></div>
    </div>
  </div>
  <div id="mapa">
    <div class="container container_1200">
      <div class="columns six">
        <h2 class="titulo linha"><span></span>LOCALIZA&Ccedil;&Atilde;O</h2>
        <h3 class="titulo times">ENCONTRE-NOS NO MAPA</h3>
        <div class="infos">
          <span class="nexabold">ATENDIMENTO Hor&aacute;rio de ver&atilde;o</span>
          <span class="nexalight">Quarta-feira &agrave; Domingo: 13h30min &agrave;s 18h<br />
        </span></div>
        <div class="infos">
          <span class="nexabold">ATENDIMENTO Hor&aacute;rio de inverno</span>
          <span class="nexalight">Quarta-feira &agrave; sexta-feira: 9h &agrave;s 17h<br />
            S&aacute;bado e Domingo: 13h &agrave;s 17h</span>
        </div>
      </div>
      <div class="columns thirteen omega offset-by-one"><img src="img/localizacao.png" width="100%" alt="Encontre-nos"/></div>
      <div class="clear"></div>
    </div>
  </div>
  <div><?php include("rodape.php"); ?></div>
</div>
<?php include("scripts.php"); ?>
<script type="text/javascript" src="gzip/gzip.php?arquivo=../jquery/jquery.bxslider.min.js&amp;cid=<?=$cid?>"></script> 
<script type="text/javascript">$("#slider_atracoes").bxSlider({minSlides:2,maxSlides:5,slideWidth:380,slideMargin:0,controls:!0,pager:!1}),$("#slider_noticias").bxSlider({mode:"fade",minSlides:1,maxSlides:1,nextSelector:"#slider-next",prevSelector:"#slider-prev",nextText:"safd",prevText:"sdf",pager:!1,adaptiveHeight:!1});</script>
</body>
</html>
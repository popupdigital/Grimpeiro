<?
/*********************************************************
*Controle de versao: 2.0
*********************************************************/
include("gzip/gzipHTML.php");
include_once("extranet/autoload.php");
$criteriaApoiador = new CDbCriteria();
$criteriaApoiador->order = 'idapoiador asc';
$criteriaApoiador->addCondition("ativo = 1");
$apoiadores = Apoiador::model()->findAll($criteriaApoiador);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Apoiadores - Grimpeiro - Grupo de Apoio &agrave; Gest&atilde;o do Parque Estadual das Arauc&aacute;rias</title>
<?php include("header.php"); ?>
<style type="text/css">
<? echo file_get_contents ('css/formularios.css');?>
</style>
</head>
<body class="internas apoiadores">
<div id="wrapper">
  <div id="topo"><?php include("topo.php"); ?></div>
  <div class="container container_1200 conteudo">
    <div class="titulo_pagina">
      <h2>APOIADORES</h2>
      <h3>QUEM COLABORA<br />CONOSCO</h3>
    </div>
    <div class="conteudo apoiadores_box">
      <?
        foreach($apoiadores as $apoiador){
	  ?>
        <div class="apoiadores_lista"><img src="extranet/uploads/Apoiador/<?=$apoiador->logo?>" alt="" class="rollover"/></div>
      <?
		}
	  ?>
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
  <div>
    <?php include("rodape.php"); ?>
  </div>
</div>
<?php include("scripts.php"); ?>
</body>
</html>
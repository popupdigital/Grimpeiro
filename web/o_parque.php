<?
/*********************************************************
*Controle de versao: 2.0
*********************************************************/
include("gzip/gzipHTML.php");
include_once("extranet/autoload.php");
$criteriaParque = new CDbCriteria();
$criteriaParque->order = 'idparque asc';
$criteriaParque->addCondition("ativo = 1");
$parques = Parque::model()->findAll($criteriaParque);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>O Parque - Grimpeiro - Grupo de Apoio &agrave; Gest&atilde;o do Parque Estadual das Arauc&aacute;rias</title>
<?php include("header.php"); ?>
<style type="text/css">
<?
 echo file_get_contents ('css/blueberry.css');
 echo file_get_contents ('css/accordion.css');
?>
</style>
</head>
<body class="internas parque">
<div id="wrapper">
  <div id="topo"><?php include("topo.php"); ?></div>
  <div class="container conteudo">
    <div class="titulo_pagina">
      <h2>O PARQUE</h2>
      <h3>CONHE&Ccedil;A O PARQUE<br />ESTADUAL DAS ARAUC&Aacute;RIAS</h3>
    </div>
    <div class="txt conteudo">Criado pelo Decreto n&ordm; 293, de 30 de maio de 2003, localiza-se no munic&iacute;pio de S&atilde;o Domingos, na Bacia do Rio Chapec&oacute;. A &aacute;rea de 612 hectares &eacute; exclusivamente coberta por floresta ombr&oacute;fila. &Eacute; importante ressaltar a ocorr&ecirc;ncia de duas esp&eacute;cies em extin&ccedil;&atilde;o, a arauc&aacute;ria angustif&oacute;lia (arauc&aacute;ria) e dicksonia sellowiana (xaxim). Dentro do Parque encontra-se o rio jacutinga, afluente do rio bonito. Al&eacute;m de ser um importante afluente do rio Chapec&oacute;, &eacute; respons&aacute;vel pelo abastecimento de &aacute;gua do munic&iacute;pio de S&atilde;o Domingos. O Parque Estadual das Arauc&aacute;rias &eacute; a primeira unidade de conserva&ccedil;&atilde;o de arauc&aacute;rias sob a responsabilidade do Governo do Estado.</div>
    <div class="accordion_conteudo">
      <?
        foreach($parques as $parque){
	  ?>
      <div>
        <div class="accordionButton"><?=htmlentities($parque->titulo);?></div>
        <div class="accordionContent">
		  <?
        	if(is_file($arquivo = 'extranet/uploads/Parque/'.str_replace('.','.',$parque->foto_destaque))){
          ?>
        	<div class="imagem"><img width="100%" src="<?=$arquivo?>" alt="<?=htmlentities($parque->titulo);?>"/></div>
        	<?
       		  }
	        ?>
    	    <?
        	  $fotos = $parque->galeria->getGallery()->galleryPhotos; 
	          $total = count($fotos);
	          if ($total>0){
	        ?>
    		  <div class="blueberry">
		        <ul class="slides">
        		  <?
		 			if(is_array($fotos)){
			          foreach($fotos as $i => $foto){
		          ?>
			        <li><img src="extranet/gallery/<?=$foto->id?>medium.jpg" width="100%" alt="<?=htmlentities($parque->titulo);?>"/></li>
		          <?
			          }
		   		    }
		          ?>
		        </ul> 
	          </div>
			<?
   	          }
	        ?>
            <?
              if ($parque->video!=NULL){
			?>
              <div class="videoWrapper"><iframe width="560" height="315" src="<?=($parque->video);?>" frameborder="0" allowfullscreen></iframe></div>
            <?
		  	}
		    ?>
          <div class="txt"><?=($parque->texto);?></div>
        </div>
      </div>
      <?
		}
	  ?>
    </div>
  </div>
  <div>
    <?php include("rodape.php"); ?>
  </div>
</div>
<?php include("scripts.php"); ?>
<script type="text/javascript" src="gzip/gzip.php?arquivo=../jquery/jquery.blueberry.min.js&amp;cid=<?=$cid?>"></script> 
<script type="text/javascript">
  $(window).load(function() {
	$('.blueberry').blueberry();
  });
</script>
</body>
</html>
<?
/*********************************************************
*Controle de versao: 2.0
*********************************************************/
include("gzip/gzipHTML.php");
include_once("extranet/autoload.php");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Agendar Visita - Grimpeiro - Grupo de Apoio &agrave; Gest&atilde;o do Parque Estadual das Arauc&aacute;rias</title>
	<?php include("header.php"); ?>
	<style type="text/css">
	<? echo file_get_contents ('css/formularios.css');?>
</style>
</head>
<body class="internas noticias">
	<div id="wrapper">
		<div id="topo"><?php include("topo.php"); ?></div>
		<div class="container conteudo">
			<div class="titulo_pagina">
				<h2>AGENDAR VISITA</h2>
				<h3>EXPLORE AS BELEZAS<br />DE NOSSO PARQUE</h3>
			</div>
			<div class="conteudo ">
				<div class="sucesso_msg columns sixteen u-full-width" style="text-align:center"> <strong>Agendamento solicitado com sucesso. Obrigado!</strong> </div>

			</div>
		</div>
		<div>
			<?php include("rodape.php"); ?>
		</div>
	</div>
	<?php include("scripts.php"); ?>
</body>
</html>

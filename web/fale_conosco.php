<?
/*********************************************************
*Controle de versao: 2.0
*********************************************************/
include("gzip/gzipHTML.php"); 
include_once("extranet/autoload.php");
$model = new Contato();
if(is_array($_POST['Contato'])){
	$model->attributes = $_POST['Contato'];
	$model->data = date('d/m/Y H:i:s');
	$model->ip = $_SERVER['REMOTE_ADDR'];
	
	if($model->save()){
		$model = new Contato();
		$sucesso = 1;
		header("Location: contato?sucesso=1");
	}
	}
$erro = CHtml::errorSummary($model);
$form = new CActiveForm();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Contato - Grimpeiro - Grupo de Apoio &agrave; Gest&atilde;o do Parque Estadual das Arauc&aacute;rias</title>
<?php include("header.php"); ?>
<style type="text/css">
<? echo file_get_contents ('css/formularios.css');
?>
</style>
</head>
<body class="internas contato">
<div id="wrapper">
  <div id="topo">
    <?php include("topo.php"); ?>
  </div>
  <div class="container conteudo">
    <div class="titulo_pagina">
      <h2>CONTATO</h2>
      <h3>COMO PODEMOS<br />
        AJUD&Aacute;-LO?</h3>
    </div>
    <div class="conteudo">
      <div class="columns eight">
        <h3>ATENDIMENTO - HOR&Aacute;RIO DE VER&Atilde;O</h3>
        <div class="txt">
          <p><span class="nexalight">Quarta-feira &agrave; Domingo: 13h30min &agrave;s 18h<br />
          </span></p>
        </div>
      </div>
      <div class="columns eight">
        <h3>ATENDIMENTO - HOR&Aacute;RIO DE INVERNO</h3>
        <div class="txt">
          <p>Quarta-feira &agrave; sexta-feira: 9h &agrave;s 17h<br />
            S&aacute;bado e Domingo: 13h &agrave;s 17h</p>
        </div>
      </div>
      <div class="clear"></div>
    </div>
    <div class="conteudo formulario">
      <?
	    if(!empty($erro)){
	  ?>
      <div class="error margin20 center columns sixteen u-full-width">
        <?=$erro;?>
      </div>
      <?
	    }
	    if($_GET['sucesso'] == 1){
	  ?>
      <div class="sucesso_msg columns sixteen u-full-width" style="text-align:center"> <strong>Contato enviado com sucesso. Obrigado!</strong> </div>
      <?
	    }
	  ?>
      <div class="clear"></div>
      <form id="form_contato" name="form_contato" method="post" action="contato" class="">
        <input type="hidden"  name="grava" value="1" />
        <?php echo $form->textField($model,'nome',array('class'=>'columns eight u-full-width','maxlength'=>100,'placeholder'=>'NOME')); ?> <?php echo $form->textField($model,'email',array('class'=>'columns eight u-full-width','maxlength'=>100,'placeholder'=>'E-MAIL')); ?> <?php echo $form->textField($model,'telefone',array('class'=>'columns eight u-full-width','maxlength'=>100,'placeholder'=>'TELEFONE')); ?> <?php echo $form->textField($model,'assunto',array('class'=>'columns eight u-full-width','maxlength'=>100,'placeholder'=>'ASSUNTO')); ?> <?php echo $form->textArea($model,'mensagem',array('rows'=>'6','cols'=>'40','placeholder'=>'MENSAGEM','class'=>'columns sixteen u-full-width')); ?>
        <div class="clear"></div>
        <button name="enviar" type="submit" value="ENVIAR" class="u-pull-right">ENVIAR</button>
        <div class="clear"></div>
      </form>
    </div>
  </div>
  <div>
    <?php include("rodape.php"); ?>
  </div>
</div>
<?php include("scripts.php"); ?>
</body>
</html>
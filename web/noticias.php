<?
/*********************************************************
*Controle de versao: 2.0
*********************************************************/
include("gzip/gzipHTML.php");
include_once("extranet/autoload.php");
$criteriaNoticia = new CDbCriteria();
$criteriaNoticia->order = 'data desc';
$criteriaNoticia->addCondition("ativo = 1");
$noticias = new CActiveDataProvider('Noticia', array(
	'criteria'=> $criteriaNoticia,
	'pagination'=>array(
        'pageSize'=> 10,
		'pageVar' => 'pagina',
    ),
));
$_GET['pagina'] = is_numeric($_GET['pagina']) ? $_GET['pagina']+1 : 0;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Not&iacute;cias - Grimpeiro - Grupo de Apoio &agrave; Gest&atilde;o do Parque Estadual das Arauc&aacute;rias</title>
<?php include("header.php"); ?>
</head>
<body class="internas noticias">
<div id="wrapper">
  <div id="topo"><?php include("topo.php"); ?></div>
  <div class="container conteudo">
    <div class="titulo_pagina">
      <h2>NOT&Iacute;CIAS</h2>
      <h3>ACOMPANHE<br />NOSSAS NOVIDADES</h3>
    </div>
    <div class="conteudo">
      <?
        foreach($noticias->data as $noticia){
	  ?>
        <h2 class="noticias_lista columns eight">
          <a href="noticia/<?=$noticia->idnoticia;?>/<?=Util::removerAcentos($noticia->titulo)?>">
          <?
			if(is_file($arquivo = 'extranet/uploads/Noticia/'.str_replace('.','.',$noticia->foto_destaque))){
		  ?>
            <img src="<?=$arquivo?>" width="100%" class="block" alt="<?=$noticia->titulo;?>"/>
          <?
			}
		  ?>
            <span><i><?=$noticia->titulo;?></i></span>
          </a>
        </h2>
      <?
		}
	  ?>
      <div class="clear"></div>
      <div class="paginacao">
		<?
          $link_paginaca = (isset($_GET['chave']) && $_GET['chave'] != "") ? '&chave='.$_GET['chave'] : '';
          if($noticias->pagination->currentPage-5 >= 0){
        ?>
          <a href="noticias?pagina=1<?=$link_paginaca;?>"> 1 </a> <a>...</a>
        <? 
          }for($i=4;$i>=1;$i--){
        	if($noticias->pagination->currentPage-$i >= 0){
        ?>
          <a href="noticias?pagina=<?=$noticias->pagination->currentPage-$i ;?><?=$link_paginaca;?>"><?=$noticias->pagination->currentPage-$i+1 ;?></a>
        <? 
        	}
          }
        ?>
          <a class="ativado"><?=$noticias->pagination->currentPage+1;?></a>
        <?
          for($i=$noticias->pagination->currentPage+1;$i <= $noticias->pagination->currentPage+5 && $i <= $noticias->pagination->pageCount-1;$i++){
        ?>
          <a href="noticias?pagina=<?=$i;?><?=$link_paginaca;?>"><?=$i+1?></a>
        <? 
          }if($noticias->pagination->currentPage+5 < $noticias->pagination->pageCount-1){
        ?>
          <a>...</a> <a href="noticias?pagina=<?=$noticias->pagination->pageCount-1;?><?=$link_paginaca;?>"><?=$noticias->pagination->pageCount?></a>
        <? 
          }
        ?>
      </div>    </div>
    <div class="clear"></div>
  </div>
  <div>
    <?php include("rodape.php"); ?>
  </div>
</div>
<?php include("scripts.php"); ?>
</body>
</html>
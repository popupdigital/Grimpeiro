<?
/*********************************************************
*Controle de versao: 2.0
*********************************************************/
include("gzip/gzipHTML.php");
include_once("extranet/autoload.php");
$criteriaSobre = new CDbCriteria();
$criteriaSobre->order = 'idsobre asc';
$criteriaSobre->addCondition("ativo = 1");
$sobres = Sobre::model()->findAll($criteriaSobre);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Sobre n&oacute;s - Grimpeiro - Grupo de Apoio &agrave; Gest&atilde;o do Parque Estadual das Arauc&aacute;rias</title>
<?php include("header.php"); ?>
<style type="text/css">
<?
 echo file_get_contents ('css/blueberry.css');
 echo file_get_contents ('css/accordion.css');
?>
</style>
</head>
<body class="internas">
<div id="wrapper">
  <div id="topo">
    <?php include("topo.php"); ?>
  </div>
  <div class="container conteudo">
    <div class="titulo_pagina">
      <h2>SOBRE N&Oacute;S</h2>
      <h3>CONHE&Ccedil;A O GRUPO DE APOIO &Agrave; GEST&Atilde;O DO PARQUE ESTADUAL DAS ARAUC&Aacute;RIAS</h3>
    </div>
    <div class="accordion_conteudo">
      <?
        foreach($sobres as $sobre){
	  ?>
      <div>
        <div class="accordionButton"><?=htmlentities($sobre->titulo);?></div>
        <div class="accordionContent">
		  <?
        	if(is_file($arquivo = 'extranet/uploads/Sobre/'.str_replace('.','.',$sobre->foto_destaque))){
          ?>
        	<div class="imagem"><img width="100%" src="<?=$arquivo?>" alt="<?=htmlentities($sobre->titulo);?>"/></div>
          <?
       		}
	        $fotos = $sobre->galeria->getGallery()->galleryPhotos; 
	        $total = count($fotos);
	        if ($total>0){
	      ?>
          <div class="blueberry">
            <ul class="slides">
              <?
                if(is_array($fotos)){
                  foreach($fotos as $i => $foto){
              ?>
                <li><img src="extranet/gallery/<?=$foto->id?>medium.jpg" width="100%" alt="<?=htmlentities($sobre->titulo);?>"/></li>
              <?
                  }
                }
              ?>
            </ul> 
          </div>
			<?
   	          }
              if ($sobre->video!=NULL){
			?>
              <div class="videoWrapper"><iframe width="560" height="315" src="<?=($sobre->video);?>" frameborder="0" allowfullscreen></iframe></div>
            <?
		  	  }
		    ?>
          <div class="txt"><?=($sobre->texto);?></div>
        </div>
      </div>
      <?
		}
	  ?>
    </div>
  </div>
  <div>
    <?php include("rodape.php"); ?>
  </div>
</div>
<?php include("scripts.php"); ?>
<script type="text/javascript" src="gzip/gzip.php?arquivo=../jquery/jquery.blueberry.min.js&amp;cid=<?=$cid?>"></script> 
<script type="text/javascript">
  $(window).load(function() {
	$('.blueberry').blueberry();
  });
</script>
</body>
</html>
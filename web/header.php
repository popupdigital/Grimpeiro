<?
$cid = time();
?>
<meta name="robots" content="index,follow"/><meta http-equiv="content-language" content="pt-br"/><meta name="resource-type" content="document"/><meta name="DocumentCountryCode" content="br"/><meta name="classification" content="internet"/><meta name="distribution" content="global"/><meta name="rating" content="general"/><meta name="robots" content="all"/><meta name="author" content="Alavanca Sites e Sistemas | www.alavanca.digital"/><meta name="reply-to" content="atendimento@alavanca.digital"/><meta name="custodian" content="atendimento@alavanca.digital"/><meta name="language" content="pt-br" /><meta name="viewport" content="width=device-width, initial-scale=1" /><meta name="format-detection" content="telephone=no"/>
<?php include_once("RequestManager.php");$request_manager = new RequestManager();$base_url = $request_manager->getBaseUrl($absolute=true).'/';$server = $_SERVER['SERVER_NAME']; ?><base href="<?=$base_url?>"/><?php include("header_nocache.php"); ?>
<meta property="og:title" content="Grimpeiro - Grupo de Apoio &agrave; Gest&atilde;o do Parque Estadual das Arauc&aacute;rias" />
<meta property="og:description" content="Criado pelo Decreto n� 293, de 30 de maio de 2003, localiza-se no munic�pio de S�o Domingos, na Bacia do Rio Chapec�." />
<meta property="og:type" content="website" />
<meta property="og:url" content="http://www.grimpeiro.com.br" />
<meta property="og:image" content="caminho absoluto da imagem que ir� aparecer" />
<meta property="og:site_name" content="Grimpeiro - Grupo de Apoio &agrave; Gest&atilde;o do Parque Estadual das Arauc&aacute;rias" />
<meta name="Title" content="Grimpeiro - Grupo de Apoio &agrave; Gest&atilde;o do Parque Estadual das Arauc&aacute;rias"/>
<meta name="reply-to" content="grimpeiro@grimpeiro.com.br"/>
<meta name="description" content="Criado pelo Decreto n� 293, de 30 de maio de 2003, localiza-se no munic�pio de S�o Domingos, na Bacia do Rio Chapec�."/>
<meta name="keywords" content="Arauc�rias, Grimpeiro, Parque estadual, S�o Domingos, Santa Carina, Reserva florestal"/>
<meta name="copyright" content="Copyright 2021"/>
<meta name="DC.date.created" content="2017-05-20" />
<meta name="URL" content="http://www.grimpeiro.com.br" />
<link rel="stylesheet" type="text/css" href="gzip/gzip.php?arquivo=../css/calendario.css&amp;cid=<?=$cid?>"/>
<link rel="stylesheet" type="text/css" href="gzip/gzip.php?arquivo=../css/all.css&amp;cid=<?=$cid?>"/>
<style type="text/css"><?php echo file_get_contents ('css/all.css');?><?php echo file_get_contents ('css/calendario.css');?></style>
<?php include("analytics.php"); ?>

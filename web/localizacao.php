<?
/*********************************************************
*Controle de versao: 2.0
*********************************************************/
include("gzip/gzipHTML.php"); 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Localiza&ccedil;&atilde;o - Grimpeiro - Grupo de Apoio &agrave; Gest&atilde;o do Parque Estadual das Arauc&aacute;rias</title>
<?php include("header.php"); ?>
</head>
<body class="internas localizacao">
<div id="wrapper">
  <div id="topo">
    <?php include("topo.php"); ?>
  </div>
  <div class="conteudo titulo_pagina">
    <h2>LOCALIZA&Ccedil;&Atilde;O</h2>
    <h3>ENCONTRE-NOS<br />
      NO MAPA</h3>
  </div>
  <div class="conteudo google-maps"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d24027.508037663443!2d-52.589613881356954!3d-26.465061148496417!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94e536d545223c6b%3A0xce156f6f4905956e!2sGrimpeiro+-+Grupo+de+Apoio+%C3%A0+Gest%C3%A3o+do+Parque+Estadual+das+Arauc%C3%A1rias!5e0!3m2!1spt-BR!2sbr!4v1501790860578" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe></div>
    <div class="container conteudo center">
      <div class="columns eight center">
        <h3>ATENDIMENTO - HOR&Aacute;RIO DE VER&Atilde;O</h3>
        <div class="txt center">
          <p><span class="nexalight">Quarta-feira &agrave; Domingo: 13h30min &agrave;s 18h<br />
          </span></p>
        </div>
      </div>
      <div class="columns eight center">
        <h3>ATENDIMENTO - HOR&Aacute;RIO DE INVERNO</h3>
        <div class="txt center">
          <p>Quarta-feira &agrave; sexta-feira: 9h &agrave;s 17h<br />
S&aacute;bado e Domingo: 13h &agrave;s 17h</p>
        </div>
      </div>
      <div class="clear"></div>
    </div>  <div class="clear"></div>
  <div>
    <?php include("rodape.php"); ?>
  </div>
</div>
<?php include("scripts.php"); ?>
</body>
</html>
<?php
include_once("extranet/autoload.php");

if($_GET['idlocal']!=''){
	$idlocal = $_GET['idlocal'];
}
$local = Local::model()->findByPk($idlocal);
if($_GET['data']!=''&&$_GET['data']!='undefined'){
	$data = $_GET['data'];
}else{
	if(!isset($data)){
		$data = date('Y-m-d');
	}
}
$dia = date('w', strtotime($data));

$criteria = new CDbCriteria();
$criteria->addCondition("idlocal = '".$idlocal."'");
$criteria->addCondition("dia_semana = '".$dia."'");
$horarios = Horario::model()->findAll($criteria);


$dados_consulta = array(
	'idlocal'=>$idlocal,
	'idhorario'=>'',
	'data'=>$data,
);
?>
<?php
$has_horario = false;
if($horarios){
	foreach ($horarios as $horario) {
		$dados_consulta['idhorario'] = $horario->idhorario;

		if(Reserva::model()->temDisponibilidade($dados_consulta)){

			if(!$has_horario){
				?>
				<option value="">ESCOLHA O HORÁRIO</option>
				<?php
			}
			$has_horario = true;
			?>

			<option value="<?=$horario->idhorario?>" <?=$horario->idhorario==$_GET['idhorario']?"selected":''?> >
				<?=substr($horario->hora_inicio, 0, 5)?><?php /*?> - <?=$horario->hora_termino?><?php */?>
			</option>
			<?php
		}

	}

}
if(!$has_horario){
	?>
	<option value="">Nenhum horario disponível</option>
	<?php
}
?>

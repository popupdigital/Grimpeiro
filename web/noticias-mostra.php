<?
/*********************************************************
*Controle de versao: 2.0
*********************************************************/
include("gzip/gzipHTML.php");
include_once("extranet/autoload.php");
$noticia = Noticia::model()->findByPk($_GET['noticia'],array('condition' => "ativo = '1'")); 
if(!is_object($noticia)){
	echo "P�gina inexistente!"; exit; 
} 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?=($noticia->titulo)?> - Grimpeiro - Grupo de Apoio &agrave; Gest&atilde;o do Parque Estadual das Arauc&aacute;rias</title>
<?php include("header.php"); ?>
<style type="text/css">
<?
 echo file_get_contents ('css/fancybox.css');
?>
</style>
</head>
<body class="internas noticias_mostra">
<div id="wrapper">
  <div id="topo"><?php include("topo.php"); ?></div>
  <div class="container container_1200 conteudo">
    <div class="titulo_pagina">
      <h2>NOT&Iacute;CIAS</h2>
      <h3><?=($noticia->titulo)?></h3>
    </div>
    <div class="conteudo">
      <div class="columns twelve offset-by-one">
          <?
			if(is_file($arquivo = 'extranet/uploads/Noticia/'.str_replace('.','.',$noticia->foto_destaque))){
		  ?>
            <img src="<?=$arquivo?>" width="100%" alt="<?=($noticia->titulo)?>"/>
          <?
			}
		  ?>
		<div class="margin30 addthis_inline_share_toolbox"></div>
        <div class="data margin30"><?=$noticia->data?></div>
        <div class="txt"><?=($noticia->texto)?></div>
        <div class="miniaturas">
        <?
          $fotos = $noticia->galeria->getGallery()->galleryPhotos; 
          if(is_array($fotos)){
            $total = count($fotos);
            foreach($fotos as $i => $foto){
        ?>
          <div class="columns three omega"><a href="extranet/gallery/<?=$foto->id?>.jpg" class="fancybox" data-fancybox-group="gallery"><img src="extranet/gallery/<?=$foto->id?>.jpg" width="100%" alt="<?=($noticia->titulo)?>" class="rollover"/></a></div>
	    <?
			}
		  }
		?>
        </div>
      </div>
      <div class="columns six offset-by-one noticias_outras">
        <h3>OUTRAS NOT&Iacute;CIAS</h3>
		  <?
			$criteriaOutras = new CDbCriteria();
			$criteriaOutras->addCondition("ativo = 1");
			$criteriaOutras->limit = 10;
			$outras = Noticia::model()->findAll($criteriaOutras);
            foreach($outras as $outra){
		  ?>
		<div class="noticias_outras_lista">
          <h2><a href="noticia/<?=$outra->idnoticia;?>/<?=Util::removerAcentos($outra->titulo)?>"><?=($outra->titulo)?></a></h2>        
          <div class="data margin10"><?=$outra->data?></div>
        </div>
        <?
		  }
	    ?>
      </div>
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
  <div>
    <?php include("rodape.php"); ?>
  </div>
</div>
<?php include("scripts.php"); ?>
<script type="text/javascript" src="gzip/gzip.php?arquivo=../jquery/jquery.fancybox.min.js&amp;cid=<?=$cid?>"></script> 
<script type="text/javascript" src="gzip/gzip.php?arquivo=../jquery/jquery.mousewheel-3.0.6.pack.js&amp;cid=<?=$cid?>"></script> 
<script type="text/javascript">$(document).ready(function() {$('.fancybox').fancybox();	});</script>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-58caefe87698092f"></script> 
</body>
</html>